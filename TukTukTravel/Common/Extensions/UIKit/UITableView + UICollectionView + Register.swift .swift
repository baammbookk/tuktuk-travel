//
//  UITableView + UICollectionView + Register.swift .swift
//  TukTukTravel
//
//  Created by Rizabek on 17.08.2021.
//

import UIKit

extension UICollectionView {
  
  func register(cellTypes: [UICollectionViewCell.Type]) {
    
    cellTypes.forEach {
      let reuseIdentifier = $0.className
      register($0, forCellWithReuseIdentifier: reuseIdentifier)
    }
  }
}

extension UITableView {
  
  func register(cellTypes: [UITableViewCell.Type]) {
    
    cellTypes.forEach {
      let reuseIdentifier = $0.className
      register($0, forCellReuseIdentifier: reuseIdentifier)
    }
  }
}

protocol ReusableView: AnyObject {
  static var identifier: String { get }
}

final class UITableViewCellWithIdentifire: UITableViewCell {
  static var nib: UINib {
    return UINib(nibName: identifier, bundle: nil)
  }
  
  static var identifier: String {
    return String(describing: self)
  }
}

final class UICollectionViewCellWithIdentifire: UICollectionViewCell {
  static var nib: UINib {
    return UINib(nibName: identifier, bundle: nil)
  }
  
  static var identifier: String {
    return String(describing: self)
  }
}

extension ReusableView where Self: UIView {
  static var identifier: String {
    return (NSStringFromClass(self) as NSString).lastPathComponent.components(separatedBy: ".").last!
  }
}

extension UICollectionViewCell: ReusableView {}

extension UITableViewCell: ReusableView {}
