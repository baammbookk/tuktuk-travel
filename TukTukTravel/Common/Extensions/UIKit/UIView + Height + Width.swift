//
//  UIView + Height + Width.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit.UIView

extension UIView {
    var width: CGFloat { bounds.width }
    var height: CGFloat { bounds.height }
}
