//
//  AppState.swift
//  TukTukTravel
//
//  Created by Rizabek on 06.08.2021.
//

import ReSwift

struct MainState: Equatable {
  var originCityModel: OriginCityModel?
  var destinationCityModel: DestinationCityModel?
  var departureDate: String = ""
  var returnDate: String = ""
  var searching: String = ""
  var adultCount: Int = 1
  var childrenCount: Int = 0
  var infantsCount: Int = 0
  var travelClass: TravelClass = .economy
  var appState: AppState = .loading
  var appAnimationState: AppAnimationState = .stop
  var search: SearchState = .origin
  var searchResult: SearchModel = []
  var flightOffers: FlightOffer?
}

enum AppState: Equatable {
  case loading
  case error
  case flightOffers(FlightOffer)
}

enum AppAnimationState: Equatable {
  case start
  case stop
}

enum SearchState: Equatable {
  case destination
  case origin
}

struct RouteState: Equatable {
  var state: RoutingState = .main
}

enum RoutingState: Action {
  case main
  case passengersView
  case calendarView
  case searchView
  case flightOfferView
  case pop
  case popToRoot
  case dismiss
}
