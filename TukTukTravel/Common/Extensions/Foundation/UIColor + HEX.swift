//
//  UIColor + HEX.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit.UIColor

extension UIColor {
  
  convenience init?(hex: String, alpha: CGFloat = 1.0) {
    var string = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if string.hasPrefix("#") {
      string.remove(at: string.startIndex)
    }
    
    if string.count != 6 {
      return nil
    }
    
    var rgbValue: UInt64 = 0
    Scanner(string: string).scanHexInt64(&rgbValue)
    
    let redValue = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
    let greenValue = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
    let blueValue = CGFloat(rgbValue & 0x0000FF) / 255.0
    self.init(red: redValue, green: greenValue, blue: blueValue, alpha: alpha)
  }
}
