//
//  UserDefaults.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import Foundation

@propertyWrapper
struct UserDefault<T: Codable> {
  private let key: String
  private let defaultValue: T
  
  init(key: String, defaultValue: T) {
    self.key = key
    self.defaultValue = defaultValue
  }
  
  var wrappedValue: T {
    get {
      guard let data = UserDefaults.standard.object(forKey: key) as? Data else {
        return defaultValue
      }
      
      let value = try? JSONDecoder().decode(T.self, from: data)
      return value ?? defaultValue
    }
    set {
      let data = try? JSONEncoder().encode(newValue)
      UserDefaults.standard.set(data, forKey: key)
    }
  }
}

extension UserDefaults {
  @UserDefault(key: "originCityModel", defaultValue: OriginCityModel(cityName: "Moscow", cityCode: "MOW"))
  static var originCityModel: OriginCityModel
  
  @UserDefault(key: "destinationCityModel", defaultValue: DestinationCityModel(cityName: "Makhachkala", cityCode: "MCX"))
  static var destinationCityModel: DestinationCityModel
}
