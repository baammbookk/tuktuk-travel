//
//  Constants.swift
//  TukTukTravel
//
//  Created by Rizabek on 23.08.2021.
//

import Foundation

enum ServiceConstants {
  static let searchCityUrl = "https://autocomplete.travelpayouts.com/places2"
  static let flightOffersUrl = "https://test.api.amadeus.com/v2/shopping/flight-offers"
  static let authorizationUrl = "https://test.api.amadeus.com/v1/security/oauth2/token"
  static let authorizationClientId = "oGprUnsFWoGnUeFSlykW0a4HPpMm423B"
  static let authorizationClientSecret = "YWCXPooZ1vlGQIwq"

  static let requestTimeout: TimeInterval = 10
  static let resourceTimeout: TimeInterval = 10
  static let validCodes = (200 ..< 300)
}
