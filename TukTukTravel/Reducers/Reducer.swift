//
//  MainReducer.swift
//  TukTukTravel
//
//  Created by Rizabek on 06.08.2021.
//

import Foundation
import ReSwift

func routeReducer(action: Action, state: RouteState?) -> RouteState {
  var state = state ?? RouteState()
  switch action {
  case let route as RoutingState:
    state.state = route
  default: ()
  }
  return state
}

func appReducer(action: Action, state: MainState?) -> MainState {
    var state = state ?? MainState()
    switch action {
    case let main as MainActions:
        switch main {
        case .didChangeFlightClass(let index):
          switch index {
          case 0: state.travelClass = .economy
          case 1: state.travelClass = .business
          case 2: state.travelClass = .first
          default: break
          }
        case .adultCounterIncrease:
          state.adultCount += 1
        case .adultCounterDecrease:
          state.adultCount -= 1
          if state.adultCount < 1 {
            state.adultCount = 1
          }
        case .childrenCounterIncrease:
          state.infantsCount += 1
        case .childrenCounterDecrease:
        state.infantsCount -= 1
        if state.infantsCount < 0 {
          state.infantsCount = 0
        }
        case .unplaceChildrenCounterIncrease:
          state.childrenCount += 1
        case .unplaceChildrenCounterDecrease:
          state.childrenCount -= 1
          if state.childrenCount < 0 {
            state.childrenCount = 0
          }
        case .didSelectDate(let fromDate, let toDate):
          state.departureDate = fromDate
          state.returnDate = toDate
        case .reverseCities(let from, let to):
          UserDefaults.originCityModel = OriginCityModel(cityName: to.cityName, cityCode: to.cityCode)
          UserDefaults.destinationCityModel = DestinationCityModel(cityName: from.cityName, cityCode: from.cityCode)
          state.originCityModel = OriginCityModel(cityName: to.cityName, cityCode: to.cityCode)
          state.destinationCityModel = DestinationCityModel(cityName: from.cityName, cityCode: from.cityCode)
        case .didSetOriginCity(let value):
          UserDefaults.originCityModel = value
          state.originCityModel = value
        case .didSetDestinationCity(let value):
          UserDefaults.destinationCityModel = value
          state.destinationCityModel = value
        case .readySearch(let value):
          state.search = value
        case .searchCity(let query):
          state.searching = query
        case .updateSearchResult(let result):
          state.searchResult = result
        case .loadedFlightOffers(let result):
          state.flightOffers = result
        case .startAnimation:
          state.appAnimationState = .start
        case .stopAnimation:
          state.appAnimationState = .stop
        }
    default:
        ()
    }
    return state
}
