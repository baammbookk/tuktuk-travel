//
//  UIView + Borders.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit.UIView

extension UIView {
  var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  var borderColor: UIColor? {
    get {
      return UIColor(cgColor: layer.borderColor ?? UIColor.clear.cgColor)
    }
    set {
      layer.borderColor = newValue?.cgColor
    }
  }
  
  var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
      layer.masksToBounds = false
    }
  }
  
  var maskToBounds: Bool {
    get {
      return layer.masksToBounds
    }
    set {
      layer.masksToBounds = newValue
    }
  }
}
