//
//  Images.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit.UIImage

struct Images {
  
  /// locationPin
  static let locationPin = UIImage(named: "locationPin")
  
  /// calendarIcon
  static let calendarIcon = UIImage(named: "calendarIcon")
  
  /// userIcon
  static let userIcon = UIImage(named: "userIcon")
  
  /// reverseArrowIcon
  static let reverseArrowIcon = UIImage(named: "reverseArrowIcon")
  
  /// plus
  static let plus = UIImage(named: "plus")
  
  /// plusSelected
  static let plusSelected = UIImage(named: "plusSelected")
  
  /// minus
  static let minus = UIImage(named: "minus")
  
  /// minusSelected
  static let minusSelected = UIImage(named: "minusSelected")
}
