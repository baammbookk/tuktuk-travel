//
//  SearchCityViewController.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit
import ReSwift

final class SearchViewController: UIViewController, PanModalPresentable, StoreSubscriber {
  
  // MARK: - Properties
  
  var panScrollable: UIScrollView? {
    return tableView
  }
  
  var longFormHeight: PanModalHeight {
    return .maxHeight
  }
  
  var anchorModalToLongForm: Bool {
    return false
  }
  
  var isHapticFeedbackEnabled: Bool {
    return true
  }
  
  var showDragIndicator: Bool {
    return true
  }
  
  var searchResult: SearchModel = []
  
  // MARK: - UI Elements
  
  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    let text = mainStore.state.search == .origin ? "Откуда" : "Куда"
    label.text = text
    label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
    label.numberOfLines = 0
    label.textColor = .black
    return label
  }()
  
  private lazy var searchBar: UISearchBar = {
    let searchBar = UISearchBar()
    let attributes:[NSAttributedString.Key: Any] = [
        .foregroundColor: Colors.primary,
        .font: UIFont.systemFont(ofSize: 17, weight: .regular)
    ]
    UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
    searchBar.placeholder = "Поиск"
    searchBar.searchBarStyle = .minimal
    searchBar.searchTextField.cornerRadius = 18
    searchBar.searchTextField.clipsToBounds = true
    searchBar.searchTextField.leftView?.tintColor = Colors.primary
    searchBar.searchTextField.clearButtonMode = .always
    searchBar.sizeToFit()
    searchBar.delegate = self
    return searchBar
  }()
  
  private lazy var tableView: UITableView = {
    let tableView = UITableView()
    tableView.rowHeight = 80
    tableView.estimatedRowHeight = 80
    tableView.keyboardDismissMode = .onDrag
    tableView.separatorStyle = .none
    tableView.backgroundColor = .white
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(cellTypes: [SearchCityCell.self])
    return tableView
  }()
  
  // MARK: - LIFE CIRCLE
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addSubviews()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    searchBar.becomeFirstResponder()
    mainStore.subscribe(self)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    searchBar.resignFirstResponder()
    mainStore.unsubscribe(self)
  }
  
  // MARK: - SET UI
  
  override func updateViewConstraints() {
    titleLabel.pin.top(20).left(20).right(20).sizeToFit(.width)
    searchBar.pin.below(of: titleLabel).left(12).right(12).marginTop(10).height(44)
    tableView.pin.below(of: searchBar).left().right().bottom()
    super.updateViewConstraints()
  }
  
  fileprivate func addSubviews() {
    [titleLabel, searchBar, tableView].forEach { view.addSubview($0)}
    view.backgroundColor = .white
    view.setNeedsUpdateConstraints()
  }
    
  // MARK: - Pan Modal Presentable
  
  func shouldPrioritize(panModalGestureRecognizer: UIPanGestureRecognizer) -> Bool {
    return false
  }
  
  func willTransition(to state: PanModalPresentationController.PresentationState) {
      panModalSetNeedsLayoutUpdate()
  }
  
  // MARK: - StoreSubscriber
  
  func newState(state: MainState) {
    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      self.searchResult = state.searchResult
      self.tableView.fadeReload()
    }
  }
}

// MARK: - UISearchBarDelegate

extension SearchViewController: UISearchBarDelegate {
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    searchBar.setShowsCancelButton(true, animated: true)
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    searchBar.setShowsCancelButton(false, animated: true)
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reload(_:)), object: searchBar)
    perform(#selector(self.reload(_:)), with: searchBar, afterDelay: 0.5)
  }
  
  @objc private func reload(_ searchBar: UISearchBar) {
      guard let query = searchBar.text,
            query.trimmingCharacters(in: .whitespaces) != "" else {
        return
      }
    mainStore.dispatch(MainActions.searchCity(query))
    mainStore.dispatch(searchCityMiddleware)
  }
}

// MARK: - UITableViewDelegate & UITableViewDataSource

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return searchResult.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: SearchCityCell.identifier, for: indexPath) as? SearchCityCell else {
      return UITableViewCell()
    }
    cell.searchResult = searchResult[indexPath.row]
    return cell
  }
}
