//
//  FlightOffersService.swift
//  TukTukTravel
//
//  Created by Rizabek on 18.08.2021.
//

import Foundation

final class FlightOffersService {
  private init() {}
  static let shared = FlightOffersService()
  
  private let session: URLSession = {
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = ServiceConstants.requestTimeout
    configuration.timeoutIntervalForResource = ServiceConstants.resourceTimeout
    configuration.waitsForConnectivity = true
    let session = URLSession(configuration: configuration)
    return session
  }()
  
  func searchFlightOffer(_ completion: @escaping (Result<FlightOffer, Swift.Error>) -> Void) {
    guard let state = mainStore.state else { return }
    var baseUrl = URLComponents(string: ServiceConstants.flightOffersUrl)
    baseUrl?.queryItems = [
      URLQueryItem(name: "originLocationCode", value: state.originCityModel?.cityCode),
      URLQueryItem(name: "destinationLocationCode", value: state.destinationCityModel?.cityCode),
      URLQueryItem(name: "departureDate", value: state.departureDate),
      URLQueryItem(name: "adults", value: state.adultCount.description),
      URLQueryItem(name: "children", value: state.childrenCount.description),
      URLQueryItem(name: "infants", value: state.infantsCount.description),
      URLQueryItem(name: "travelClass", value: state.travelClass.rawValue),
      URLQueryItem(name: "nonStop", value: "true"),
      URLQueryItem(name: "currencyCode", value: "RUB")
    ]

    if !state.returnDate.isEmpty {
      baseUrl?.queryItems?.append(URLQueryItem(name: "returnDate", value: state.returnDate))
    }
    
    guard let resultUrl = baseUrl?.url,
          let token = KeychainService.appToken else {
      completion(.failure(Error(message: "failed to generate URL or acces token")))
      return
    }
    
    var request = URLRequest(url: resultUrl)
    request.httpMethod = "GET"
    request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
    
    session.dataTask(with: request) { data, response, error in
      if let error = error {
        completion(.failure(error))
        return
      }
      
      guard let response = response as? HTTPURLResponse else {
        completion(.failure(Error(message: "no response")))
        return
      }
      
      guard let data = data else {
        completion(.failure(Error(message: "no data")))
        return
      }
      
      guard ServiceConstants.validCodes ~= response.statusCode else {
        
        if response.statusCode == 401 {
          
          self.retryRequest { [weak self] isRetry in
            guard let self = self, isRetry else {
              completion(.failure(Error(message: "authorization error")))
              return
            }
            self.searchFlightOffer(completion)
          }
        }
        completion(.failure(Error(message: "status code is \(response.statusCode)")))
        return
      }
      
      do {
        let object = try JSONDecoder().decode(FlightOffer.self, from: data)
        DispatchQueue.main.async {
          completion(.success(object))
        }
      } catch let error {
        completion(.failure(error))
      }
    }.resume()
  }
  
  private func retryRequest(_ completion: @escaping (Bool) -> Void) {
    AuthorizationService.shared.authorization { result in
      switch result {
      case .success(let token):
        KeychainService.appToken = token
        completion(true)
      case .failure(let error):
        NSLog(error.localizedDescription)
        completion(false)
      }
    }
  }
}
