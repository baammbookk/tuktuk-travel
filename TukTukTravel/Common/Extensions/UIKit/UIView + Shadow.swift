//
//  UIView + Shadow.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit.UIView

extension UIView {
  func addStandartShadow() {
    layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
    layer.shadowOffset = .zero
    layer.shadowRadius = 5
    layer.shadowOpacity = 1
    layer.shouldRasterize = true
    layer.rasterizationScale = UIScreen.main.scale
  }
}
