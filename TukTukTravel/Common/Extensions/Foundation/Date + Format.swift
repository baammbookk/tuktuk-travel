//
//  Date + Format.swift
//  TukTukTravel
//
//  Created by Rizabek on 21.08.2021.
//

import Foundation

extension Date {
  func string(format: String) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = format
    return formatter.string(from: self)
  }
}
