//
//  Assembly.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit

protocol Assembly {
  func initialController() -> UIViewController
  func presentPassengersView() -> UIViewController
  func presentCalendarView() -> UIViewController
  func presentSearchView() -> UIViewController
  func presentFlightOfferView() -> UIViewController
}

final class MainAssembly: Assembly {
  
  func initialController() -> UIViewController {
    let vc = MainViewController()
    return vc
  }
  
  func presentPassengersView() -> UIViewController {
    let vc = PassengerViewController()
    vc.modalPresentationStyle = .custom
    vc.modalPresentationCapturesStatusBarAppearance = true
    vc.transitioningDelegate = PanModalPresentationDelegate.default
    return vc
  }
  
  func presentCalendarView() -> UIViewController {
    let vc = CallendarViewController()
    vc.modalPresentationStyle = .custom
    vc.modalPresentationCapturesStatusBarAppearance = true
    vc.transitioningDelegate = PanModalPresentationDelegate.default
    return vc
  }
  
  func presentSearchView() -> UIViewController {
    let vc = SearchViewController()
    vc.modalPresentationStyle = .custom
    vc.modalPresentationCapturesStatusBarAppearance = true
    vc.transitioningDelegate = PanModalPresentationDelegate.default
    return vc
  }
  
  func presentFlightOfferView() -> UIViewController {
    let vc = FlightOfferViewController()
    return vc
  }
}
