//
//  Error + Message.swift
//  TukTukTravel
//
//  Created by Rizabek on 15.08.2021.
//

import Foundation

struct Error: Swift.Error {
    let message: String
}
