//
//  String + Date.swift
//  TukTukTravel
//
//  Created by Rizabek on 15.08.2021.
//

import Foundation

extension String {
  func convertDate() -> String {
    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = "yyyy-MM-dd"
    guard let showDate = inputFormatter.date(from: self) else { return "" }
    inputFormatter.dateFormat = "dd MMMM"
    let resultString = inputFormatter.string(from: showDate)
    return resultString
  }
  
  func convertOfferDate() -> String {
    let inputFormatter = DateFormatter()
    inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    guard let showDate = inputFormatter.date(from: self) else { return "" }
    inputFormatter.dateFormat = "HH:mm"
    let resultString = inputFormatter.string(from: showDate)
    return resultString
  }
  
  func convertDuration() -> String {
    let formattedDuration = self.replacingOccurrences(of: "PT", with: "").replacingOccurrences(of: "H", with:" ч ").replacingOccurrences(of: "M", with: " мин")
    let components = formattedDuration.components(separatedBy: ":")
    var duration = ""
    for component in components {
      duration = duration.count > 0 ? duration + ":" : duration
      if component.count < 2 {
        duration += "0" + component
        continue
      }
      duration += component
    }
    
    return duration
  }

}


