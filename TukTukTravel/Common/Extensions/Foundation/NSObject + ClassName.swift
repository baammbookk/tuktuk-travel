//
//  NSObject + ClassName.swift
//  TukTukTravel
//
//  Created by Rizabek on 17.08.2021.
//

import Foundation

extension NSObject {
  
  static var className: String {
    return String(describing: self)
  }
  var className: String {
    return String(describing: type(of: self))
  }
}
