//
//  FlightOfferViewController.swift
//  TukTukTravel
//
//  Created by Rizabek on 20.08.2021.
//

import UIKit
import Lottie
import ReSwift

final class FlightOfferViewController: UIViewController, StoreSubscriber {
  
  // MARK: - Properties
  
  private let animation = Animation.named("plane")
  private let itemHeight: CGFloat = mainStore.state.returnDate.isEmpty ? 114 : 154
  private let lineSpacing: CGFloat = 20
  private let xInset: CGFloat = 20
  private let topInset: CGFloat = 10
  
  private var flightOffers: FlightOffer?
  
  // MARK: - UI Elements
  
  private lazy var searchBar: UISearchBar = {
    let searchBar = UISearchBar()
    let tapGets = UITapGestureRecognizer(target: self, action: #selector(didChangeFlight))
    searchBar.searchTextField.minimumFontSize = 12
    searchBar.searchTextField.textColor = Colors.primary
    searchBar.searchTextField.clearButtonMode = .never
    searchBar.searchTextField.leftView?.tintColor = Colors.primary
    searchBar.searchBarStyle = .minimal
    searchBar.searchTextField.isUserInteractionEnabled = false
    searchBar.searchTextField.adjustsFontSizeToFitWidth = true
    searchBar.addGestureRecognizer(tapGets)
    return searchBar
  }()
  
  private lazy var collectionView: UICollectionView = {
    let layout = VegaScrollFlowLayout()
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.contentInset.bottom = itemHeight
    layout.minimumLineSpacing = lineSpacing
    layout.sectionInset = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
    let itemWidth = UIScreen.main.bounds.width - 2 * xInset
    layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
    collectionView.backgroundColor = .white
    collectionView.collectionViewLayout.invalidateLayout()
    
    collectionView.isHidden = true
    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.register(cellTypes: [FlightOfferCell.self])

    return collectionView
  }()

  private lazy var animationView: AnimationView = {
    let animationView = AnimationView()
    animationView.contentMode = .scaleAspectFit
    animationView.backgroundColor = .white
    animationView.backgroundBehavior = .pauseAndRestore
    animationView.animation = animation
    animationView.loopMode = .loop
    return animationView
  }()

  // MARK: - LIFE CIRCLE
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addSubviews()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    mainStore.subscribe(self)
    mainStore.dispatch(flightOfferMiddleware)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    mainStore.unsubscribe(self)
  }
  
  // MARK: - SET UI
  
  override func updateViewConstraints() {
    animationView.pin.all()
    animationView.setContentCompressionResistancePriority(.fittingSizeLevel, for: .horizontal)
    searchBar.pin.top(view.pin.safeArea).left(12).right(12).marginTop(10).height(44)
    collectionView.pin.below(of: searchBar).marginTop(10).left().right().bottom()
    super.updateViewConstraints()
  }
  
  fileprivate func addSubviews() {
    [animationView, searchBar, collectionView].forEach { view.addSubview($0)}
    view.backgroundColor = .white
    view.setNeedsUpdateConstraints()
  }
  
  // MARK: - ACTIONS
  
  @objc private func didChangeFlight() {
    routeStore.dispatch(RoutingState.pop)
  }
  
  private func startAnimation() {
    self.collectionView.isHidden = true
    self.animationView.isHidden = false
    self.animationView.play()
  }
  
  private func stopAnimation() {
    self.animationView.pause()
    self.animationView.isHidden = true
    self.collectionView.isHidden = false
  }
  
  // MARK: - StoreSubscriber
  
  func newState(state: MainState) {
    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      let flightString = (state.originCityModel?.cityName ?? "") + " - " + (state.destinationCityModel?.cityName ?? "")
      let toDate = state.returnDate.convertDate()
      let toDateValue = state.returnDate.isEmpty ? "" : "- \(toDate)"
      let flightDates = state.departureDate.convertDate() + " " + toDateValue
      self.searchBar.text = flightString + ", " + flightDates
      self.flightOffers = state.flightOffers
      self.collectionView.reloadData()
      state.appAnimationState == .start ? self.startAnimation() : self.stopAnimation()
    }
  }
}

// MARK: - UICollectionViewDelegate & UICollectionViewDataSource

extension FlightOfferViewController: UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FlightOfferCell.identifier, for: indexPath) as? FlightOfferCell,
          let flightOffers = flightOffers else {
      return UICollectionViewCell()
    }
    cell.flightData = flightOffers.data[indexPath.row]
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return flightOffers?.data.count ?? 0
  }
}
