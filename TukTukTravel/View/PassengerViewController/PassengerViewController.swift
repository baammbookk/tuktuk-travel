//
//  PassengerViewController.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

import UIKit
import ReSwift

final class PassengerViewController: UIViewController, PanModalPresentable, StoreSubscriber {
  
  // MARK: - Properties
  
  var panScrollable: UIScrollView? {
    return nil
  }
  
  var longFormHeight: PanModalHeight {
    return .contentHeight(525)
  }
  
  var anchorModalToLongForm: Bool {
    return false
  }
  
  var isHapticFeedbackEnabled: Bool {
    return true
  }
  
  var showDragIndicator: Bool {
    return false
  }
  
  // MARK: - UI Elements
  
  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.text = "Пассажиры"
    label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
    label.numberOfLines = 0
    label.textColor = .black
    return label
  }()
  
  private lazy var adultTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "Взрослые"
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    label.textColor = .black
    return label
  }()
  
  private lazy var adultSubtitleLabel: UILabel = {
    let label = UILabel()
    label.text = "и дети старше 12 лет"
    label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
    label.textColor = .lightGray
    return label
  }()
  
  private lazy var adultMinusButton: UIButton = {
    let button = UIButton()
    button.setImage(Images.minus, for: .normal)
    button.setImage(Images.minusSelected, for: .selected)
    button.addTarget(self, action: #selector(decreaseAdultCount), for: .touchUpInside)
    return button
  }()
  
  private lazy var adultPlusButton: UIButton = {
    let button = UIButton()
    button.setImage(Images.plusSelected, for: .normal)
    button.addTarget(self, action: #selector(increaseAdultCount), for: .touchUpInside)
    return button
  }()
  
  private lazy var adultCountLabel: UILabel = {
    let label = UILabel()
    label.text = "1"
    label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
    label.textColor = .black
    label.textAlignment = .center
    return label
  }()
  
  private lazy var placeChildrenTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "Дети от 0 до 12 лет"
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    label.textColor = .black
    return label
  }()
  
  private lazy var placeChildrenSubtitleLabel: UILabel = {
    let label = UILabel()
    label.text = "с местом"
    label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
    label.textColor = .lightGray
    return label
  }()
  
  private lazy var placeChildrenMinusButton: UIButton = {
    let button = UIButton()
    button.setImage(Images.minus, for: .normal)
    button.setImage(Images.minusSelected, for: .selected)
    button.addTarget(self, action: #selector(decreasePlaceChildrenCount), for: .touchUpInside)
    return button
  }()
  
  private lazy var placeChildrenPlusButton: UIButton = {
    let button = UIButton()
    button.setImage(Images.plusSelected, for: .normal)
    button.addTarget(self, action: #selector(increasePlaceChildrenCount), for: .touchUpInside)
    return button
  }()
  
  private lazy var placeChildrenCountLabel: UILabel = {
    let label = UILabel()
    label.text = "1"
    label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
    label.textColor = .black
    label.textAlignment = .center
    return label
  }()
  
  private lazy var unplaceChildrenTitleLabel: UILabel = {
    let label = UILabel()
    label.text = "Дети до 2 лет"
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    label.textColor = .black
    return label
  }()
  
  private lazy var unplaceChildrenSubtitleLabel: UILabel = {
    let label = UILabel()
    label.text = "без места"
    label.font = UIFont.systemFont(ofSize: 17, weight: .regular)
    label.textColor = .lightGray
    return label
  }()
  
  private lazy var unplaceChildrenMinusButton: UIButton = {
    let button = UIButton()
    button.setImage(Images.minus, for: .normal)
    button.setImage(Images.minusSelected, for: .selected)
    button.addTarget(self, action: #selector(decreaseUnplaceChildrenCount), for: .touchUpInside)
    return button
  }()
  
  private lazy var unplaceChildrenPlusButton: UIButton = {
    let button = UIButton()
    button.setImage(Images.plusSelected, for: .normal)
    button.setImage(Images.plus, for: .selected)
    button.addTarget(self, action: #selector(increaseUnplaceChildrenCount), for: .touchUpInside)
    return button
  }()
  
  private lazy var unplaceChildrenCountLabel: UILabel = {
    let label = UILabel()
    label.text = "1"
    label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
    label.textColor = .black
    label.textAlignment = .center
    return label
  }()
  
  private lazy var flightСlassLabel: UILabel = {
    let label = UILabel()
    label.text = "Класс перелeта"
    label.font = UIFont.systemFont(ofSize: 24, weight: .bold)
    label.textColor = .black
    return label
  }()
  
  private lazy var readyButton: UIButton = {
    let button = UIButton()
    button.setTitle("Готово", for: .normal)
    button.setTitleColor(.white, for: .normal)
    button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    button.backgroundColor = Colors.primary
    button.cornerRadius = 8
    button.addTarget(self, action: #selector(didSelectReadyAction), for: .touchUpInside)
    return button
  }()
  
  private lazy var classSegmentControll: BetterSegmentedControl = {
    let segment = BetterSegmentedControl()
    segment.segments = LabelSegment.segments(withTitles: ["Эконом",
                                                          "Бизнес",
                                                          "Первый"],
                                             normalFont: UIFont.systemFont(ofSize: 17, weight: .semibold),
                                             normalTextColor: .lightGray,
                                             selectedFont: UIFont.systemFont(ofSize: 17, weight: .semibold),
                                             selectedTextColor: .white)
    segment.setOptions([.backgroundColor(Colors.background),
                        .indicatorViewBackgroundColor(Colors.primary)])
    segment.segmentCornerRadius = 20
    return segment
  }()
  
  // MARK: - LIFE CIRCLE
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addSubviews()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    mainStore.subscribe(self)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    mainStore.unsubscribe(self)
  }
  
  // MARK: - Set Layout
  
  override func updateViewConstraints() {
    titleLabel.pin.top(40).left(20).right(20).sizeToFit(.width)
    adultTitleLabel.pin.below(of: titleLabel, aligned: .left).marginTop(30).sizeToFit()
    adultSubtitleLabel.pin.below(of: adultTitleLabel, aligned: .left).marginTop(8).sizeToFit()
    adultPlusButton.pin.below(of: titleLabel).size(25).marginTop(40).right(20)
    adultMinusButton.pin.left(of: adultPlusButton, aligned: .center).size(25).marginRight(40)
    adultCountLabel.pin.horizontallyBetween(adultMinusButton, and: adultPlusButton, aligned: .center).marginHorizontal(8).sizeToFit(.width)
    
    placeChildrenTitleLabel.pin.below(of: adultSubtitleLabel, aligned: .left).marginTop(20).sizeToFit()
    placeChildrenSubtitleLabel.pin.below(of: placeChildrenTitleLabel, aligned: .left).marginTop(8).sizeToFit()
    placeChildrenPlusButton.pin.below(of: adultSubtitleLabel).size(25).marginTop(35).right(20)
    placeChildrenMinusButton.pin.left(of: placeChildrenPlusButton, aligned: .center).size(25).marginRight(40)
    placeChildrenCountLabel.pin.horizontallyBetween(placeChildrenMinusButton, and: placeChildrenPlusButton, aligned: .center).marginHorizontal(8).sizeToFit(.width)
    
    unplaceChildrenTitleLabel.pin.below(of: placeChildrenSubtitleLabel, aligned: .left).marginTop(20).sizeToFit()
    unplaceChildrenSubtitleLabel.pin.below(of: unplaceChildrenTitleLabel, aligned: .left).marginTop(8).sizeToFit()
    unplaceChildrenPlusButton.pin.below(of: placeChildrenSubtitleLabel).size(25).marginTop(35).right(20)
    unplaceChildrenMinusButton.pin.left(of: unplaceChildrenPlusButton, aligned: .center).size(25).marginRight(40)
    unplaceChildrenCountLabel.pin.horizontallyBetween(unplaceChildrenMinusButton, and: unplaceChildrenPlusButton, aligned: .center).marginHorizontal(8).sizeToFit(.width)
    
    flightСlassLabel.pin.below(of: unplaceChildrenSubtitleLabel, aligned: .left).marginTop(40).sizeToFit()
    classSegmentControll.pin.below(of: flightСlassLabel, aligned: .left).right(20).marginTop(30).height(40)
    readyButton.pin.below(of: classSegmentControll, aligned: .left).right(to: classSegmentControll.edge.right).marginTop(30).height(44)
    super.updateViewConstraints()
  }
  
  // MARK: - SET UI
  
  fileprivate func addSubviews() {
    [titleLabel,
     adultTitleLabel,
     adultSubtitleLabel,
     adultMinusButton,
     adultCountLabel,
     adultPlusButton,
     placeChildrenTitleLabel,
     placeChildrenSubtitleLabel,
     placeChildrenMinusButton,
     placeChildrenCountLabel,
     placeChildrenPlusButton,
     unplaceChildrenTitleLabel,
     unplaceChildrenSubtitleLabel,
     unplaceChildrenMinusButton,
     unplaceChildrenCountLabel,
     unplaceChildrenPlusButton,
     flightСlassLabel,
     classSegmentControll,
     readyButton].forEach { view.addSubview($0)}
    view.backgroundColor = .white
    view.setNeedsUpdateConstraints()
  }
  
  // MARK: - ACTIONS
  
  @objc private func didSelectReadyAction() {
    mainStore.dispatch(MainActions.didChangeFlightClass(index: classSegmentControll.index))
    routeStore.dispatch(RoutingState.dismiss)
  }
  
  @objc private func increaseUnplaceChildrenCount() {
    unplaceChildrenMinusButton.isSelected = true
    mainStore.dispatch(MainActions.unplaceChildrenCounterIncrease)
  }
  
  @objc private func decreaseUnplaceChildrenCount() {
    mainStore.dispatch(MainActions.unplaceChildrenCounterDecrease)
  }
  
  @objc private func increasePlaceChildrenCount() {
    placeChildrenMinusButton.isSelected = true
    mainStore.dispatch(MainActions.childrenCounterIncrease)
  }
  
  @objc private func decreasePlaceChildrenCount() {
    mainStore.dispatch(MainActions.childrenCounterDecrease)
  }
  
  @objc private func increaseAdultCount() {
    adultMinusButton.isSelected = true
    mainStore.dispatch(MainActions.adultCounterIncrease)
  }
  
  @objc private func decreaseAdultCount() {
    mainStore.dispatch(MainActions.adultCounterDecrease)
  }
  
  // MARK: - StoreSubscriber
  
  func newState(state: MainState) {
    adultCountLabel.text = "\(state.adultCount)"
    placeChildrenCountLabel.text = "\(state.infantsCount)"
    unplaceChildrenCountLabel.text = "\(state.childrenCount)"
    
    if state.adultCount < 2 {
      adultMinusButton.isSelected = false
    }
    
    if state.infantsCount == 0 {
      placeChildrenMinusButton.isSelected = false
    }
    
    if state.childrenCount == 0 {
      unplaceChildrenMinusButton.isSelected = false
    }
    
    unplaceChildrenPlusButton.isUserInteractionEnabled = true
    unplaceChildrenPlusButton.isSelected = false
    if state.adultCount == state.childrenCount {
      unplaceChildrenPlusButton.isSelected = true
      unplaceChildrenPlusButton.isUserInteractionEnabled = false
    }
  }
}
