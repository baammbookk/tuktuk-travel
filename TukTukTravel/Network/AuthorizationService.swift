//
//  AuthorizationService.swift
//  TukTukTravel
//
//  Created by Rizabek on 23.08.2021.
//

import Foundation

struct AuthorizationService {
  private init() {}
  static let shared = AuthorizationService()
  private let session = URLSession.shared
  
  func authorization(_ completion: @escaping (Result<String, Swift.Error>) -> Void) {
    
    let headers: [String: String] = [
      "Content-Type": "application/x-www-form-urlencoded"
    ]
    
    let parameters = "grant_type=client_credentials&client_id=\(ServiceConstants.authorizationClientId)&client_secret=\(ServiceConstants.authorizationClientSecret)"
    
    guard let url = URL(string: ServiceConstants.authorizationUrl),
          let httpBodyData = parameters.data(using: String.Encoding.ascii, allowLossyConversion: true) else {
      completion(.failure(Error(message: "failed to generate URL or body parameters")))
      return
    }
    
    var request = URLRequest(url: url, timeoutInterval: ServiceConstants.requestTimeout)
    request.allHTTPHeaderFields = headers
    request.httpBody = httpBodyData
    request.httpMethod = "POST"

    session.dataTask(with: request) { data, response, error in
      if let error = error {
        completion(.failure(error))
        return
      }
      
      guard let response = response as? HTTPURLResponse else {
        completion(.failure(Error(message: "no response")))
        return
      }
      
      guard let data = data else {
        completion(.failure(Error(message: "no data")))
        return
      }
      
      guard ServiceConstants.validCodes ~= response.statusCode else {
        completion(.failure(Error(message: "status code is \(response.statusCode)")))
        return
      }
      
      do {
        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
          if let token = json["access_token"] as? String {
            DispatchQueue.main.async {
              completion(.success(token))
            }
          }
        }
      } catch let error {
        completion(.failure(error))
      }
    }.resume()
  }
}
