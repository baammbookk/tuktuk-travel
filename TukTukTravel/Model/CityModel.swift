//
//  CityModel.swift
//  TukTukTravel
//
//  Created by Rizabek on 19.08.2021.
//

import Foundation

struct OriginCityModel: Codable, Equatable {
  let cityName: String
  let cityCode: String
}

struct DestinationCityModel: Codable, Equatable {
  let cityName: String
  let cityCode: String
}

