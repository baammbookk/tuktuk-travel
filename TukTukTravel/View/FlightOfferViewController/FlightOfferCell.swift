//
//  FlightOfferCell.swift
//  TukTukTravel
//
//  Created by Rizabek on 20.08.2021.
//

import UIKit

final class FlightOfferCell: UICollectionViewCell {
  
  // MARK: - Properties
  
  var flightData: FlightData? {
    didSet {
      guard let flightData = flightData else { return }
      let price: Double = Double(flightData.price.total) ?? 0
      priceLabel.text =  price.getFormattedPrice() + " ₽"
      
      let originCityCode = mainStore.state.originCityModel?.cityCode ?? ""
      let destintionCityCode = mainStore.state.destinationCityModel?.cityCode ?? ""
      let departureTime = flightData.itineraries.first?.segments.first?.departure.at.convertOfferDate() ?? ""
      let arrivalTime = flightData.itineraries.first?.segments.first?.arrival.at.convertOfferDate() ?? ""
      let durationTime = flightData.itineraries.first?.segments.first?.duration.convertDuration() ?? ""
      
      departureTimeLabel.text = departureTime + " - " + arrivalTime
      departureTravelTimeLabel.text = durationTime
      departureCityCodeLabel.text = originCityCode + "-" + destintionCityCode
      
      if !mainStore.state.returnDate.isEmpty {
        let returnDepartureTime = flightData.itineraries.last?.segments.first?.departure.at.convertOfferDate() ?? ""
        let returnArrivalTime = flightData.itineraries.last?.segments.first?.arrival.at.convertOfferDate() ?? ""
        let returnDurationTime = flightData.itineraries.last?.segments.first?.duration.convertDuration() ?? ""
        
        returnCityCodeLabel.text = destintionCityCode + "-" + originCityCode
        returnTimeLabel.text = returnDepartureTime + " - " + returnArrivalTime
        returnTravelTimeLabel.text = returnDurationTime
      }
      
      let carrierCode = flightData.itineraries.first?.segments.first?.carrierCode ?? ""
      
      ImageLoader.shared.loadImage("https://pics.avs.io/90/30/\(carrierCode).png") { [weak self] image, url in
        guard let self = self,
              let image = image else { return }
        self.airlineLogo.image = image
      }
    }
  }
  
  // MARK: - UI Elements
  
  private lazy var departureCityCodeLabel: UILabel = {
    let label = UILabel()
    label.textColor = .gray
    label.text = "MOW-MCX"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    return label
  }()
  
  private lazy var departureTimeLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.text = "19:05 - 12:30"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    return label
  }()
  
  private lazy var returnCityCodeLabel: UILabel = {
    let label = UILabel()
    label.textColor = .gray
    label.text = "MCX-MOW"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    label.isHidden = mainStore.state.returnDate.isEmpty
    return label
  }()
  
  private lazy var returnTimeLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.text = "19:05 - 12:30"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    label.isHidden = mainStore.state.returnDate.isEmpty
    return label
  }()
  
  private lazy var departureTransferCountLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .gray
    label.text = "Без пересадок"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    return label
  }()
  
  private lazy var departureTransferTimeLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.text = "Прямой"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    return label
  }()
  
  private lazy var returnTransferCountLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .gray
    label.text = "Без пересадок"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    label.isHidden = mainStore.state.returnDate.isEmpty
    return label
  }()
  
  private lazy var returnTransferTimeLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.text = "Прямой"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    label.isHidden = mainStore.state.returnDate.isEmpty
    return label
  }()
  
  private lazy var departureTravelTimeTitleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .gray
    label.text = "В пути"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    return label
  }()
  
  private lazy var departureTravelTimeLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.text = "2 ч 55 мин"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    return label
  }()
  
  private lazy var returnTravelTimeTitleLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .gray
    label.text = "В пути"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    label.isHidden = mainStore.state.returnDate.isEmpty
    return label
  }()
  
  private lazy var returnTravelTimeLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .left
    label.textColor = .black
    label.text = "2 ч 55 мин"
    label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    label.isHidden = mainStore.state.returnDate.isEmpty
    return label
  }()
  
  private lazy var separatorView: UIView = {
    let view = UIView()
    view.backgroundColor = .separator
    return view
  }()
  
  private lazy var priceLabel: UILabel = {
    let label = UILabel()
    label.textColor = Colors.primary
    label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
    return label
  }()
  
  private lazy var companyLogosStackView: UIStackView = {
    let stackView = UIStackView()
    stackView.distribution = .fillEqually
    stackView.axis = .horizontal
    stackView.spacing = 4
    return stackView
  }()
  
  private lazy var airlineLogo: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  // MARK: - Init
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    addSubViews()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - SET UI
  
  override func layoutSubviews() {
    super.layoutSubviews()
    layout()
  }
  
  private func addSubViews() {
    [departureCityCodeLabel,
     departureTimeLabel,
     returnCityCodeLabel,
     returnTimeLabel,
     departureTransferCountLabel,
     departureTransferTimeLabel,
     returnTransferCountLabel,
     returnTransferTimeLabel,
     departureTravelTimeTitleLabel,
     departureTravelTimeLabel,
     returnTravelTimeTitleLabel,
     returnTravelTimeLabel,
     separatorView,
     priceLabel,
     airlineLogo].forEach { contentView.addSubview($0) }
    contentView.addStandartShadow()
    contentView.cornerRadius = 8
    contentView.backgroundColor = .white
    contentView.setNeedsUpdateConstraints()
  }
      
  // MARK: - Set Layout
  
  fileprivate func layout() {
    departureCityCodeLabel.pin.topLeft(12).sizeToFit(.widthFlexible)
    departureTimeLabel.pin.below(of: departureCityCodeLabel,aligned: .left).marginTop(6).sizeToFit(.widthFlexible)
    
    returnCityCodeLabel.pin.below(of: departureTimeLabel, aligned: .left).marginTop(12).sizeToFit(.widthFlexible)
    returnTimeLabel.pin.below(of: returnCityCodeLabel,aligned: .left).marginTop(6).sizeToFit(.widthFlexible)

    departureTransferCountLabel.pin.after(of: departureTimeLabel).top(12).marginLeft(30).sizeToFit(.widthFlexible)
    departureTransferTimeLabel.pin.below(of: departureTransferCountLabel, aligned: .left).marginTop(6).sizeToFit(.widthFlexible)
    
    returnTransferCountLabel.pin.below(of: departureTransferTimeLabel, aligned: .left).marginTop(12).sizeToFit(.widthFlexible)
    returnTransferTimeLabel.pin.below(of: returnTransferCountLabel, aligned: .left).marginTop(6).sizeToFit(.widthFlexible)
    
    departureTravelTimeTitleLabel.pin.after(of: returnTransferCountLabel).top(12).marginLeft(30).sizeToFit(.widthFlexible)
    departureTravelTimeLabel.pin.below(of: departureTravelTimeTitleLabel, aligned: .left).marginTop(6).sizeToFit(.widthFlexible)
    
    returnTravelTimeTitleLabel.pin.below(of: departureTravelTimeLabel, aligned: .left).marginTop(12).sizeToFit(.widthFlexible)
    returnTravelTimeLabel.pin.below(of: returnTravelTimeTitleLabel, aligned: .left).marginTop(6).sizeToFit(.widthFlexible)
    
    priceLabel.pin.bottom(10).left(12).sizeToFit(.widthFlexible)
    separatorView.pin.above(of: priceLabel).marginBottom(12).height(0.5).left().right()

    airlineLogo.pin.after(of: priceLabel, aligned: .center).width(60).height(20).marginLeft(35)
  }
}
