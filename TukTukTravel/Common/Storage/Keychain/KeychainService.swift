//
//  KeychainService.swift
//  TukTukTravel
//
//  Created by Rizabek on 07.08.2021.
//

final class KeychainService {
  
  static var keychain = KeychainSwift(keyPrefix: "tuktuk-travel-app")
  private static let token = "app-token-key"
  
  static var appToken: String? {
    get {
      return keychain.get(token)
    }
    set {
      keychain.set(newValue!, forKey: token)
    }
  }
  
  static func cleanKeychan() {
    keychain.clear()
  }
}
